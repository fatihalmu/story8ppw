import json
from django.shortcuts import render
from django.shortcuts import reverse
from django.http import HttpResponse,HttpResponseRedirect
from .forms import subscription
from .models import Subscribe_model

BOOK_API = 'https://www.googleapis.com/books/v1/volumes?q='

response = {}

# Create your views here.
def bukurekomen_views(request):
	books = json.dumps(BOOK_API)
	response['books'] = books
	return render(request,'bukumenarik.html',response)

def subscribe_views(request):
	if request.method == "POST":
		form = subscription(request.POST)
		if form.is_valid():
			agendabaru = form.save()
			return HttpResponseRedirect(reverse('subscription'))
	else:
		form = subscription()

	return render(request, 'subscribe.html', {'form': form})

def validasi_email(request,email= None):
	email_model= Subscribe_model.objects.filter(email__iexact=email).exists()
	data = {'ada': email_model}
	if data['ada']:
		data['message'] = 'Email tersebut sudah pernah didaftarkan sebelumnya, silahkan daftar dengan email lain.'
	else:
		data['message'] = 'Email belum terdaftar, silahkan daftar dengan email ini.'
	json_data = json.dumps(data)
	return HttpResponse(json_data, content_type='application/json')
def subscribe_click (request):
	if(request.method== 'POST'):
		nama = request.POST['nama']
		email = request.POST['email']
		password = request.POST['password']
		Subscribe_model.objects.create(nama=nama,email=email,password=password)
		return HttpResponse('')
	
