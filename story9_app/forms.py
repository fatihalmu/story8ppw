from django import forms
from .models import Subscribe_model
class subscription(forms.ModelForm):
	password = forms.CharField(widget=forms.PasswordInput())
	class Meta:
		model = Subscribe_model
		fields = ['nama','email','password']
		
		widgets ={
		'nama': forms.TextInput(attrs={'class': 'form-control','type': 'text','placeholder':'Isi nama'}),
		'email': forms.EmailInput(attrs={'class': 'form-control','type': 'text','placeholder':'Isi email'}),
		'password': forms.TextInput(attrs={'class': 'form-control','type': 'text','placeholder':'Isi password'}),

		}

		labels = {
            'nama': 'Nama Kamu',
            'email': 'Email Kamu',
            'password': 'Password Kamu',
        }