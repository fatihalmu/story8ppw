from django.urls import path
from .views import bukurekomen_views,subscribe_views,validasi_email,subscribe_click
	#url for app
urlpatterns = [
	path('',bukurekomen_views,name='bukurekomendasi'),
	path('validasi/<email>/',validasi_email,name='validasi'),
	path('subscribe/',subscribe_views,name='subscription'),
	path('subscribeclick/',subscribe_click,name='subsclick'),
	]
