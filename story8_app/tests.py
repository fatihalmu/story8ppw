from django.test import TestCase, Client
from django.urls import resolve
from .views import homepage
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
class Lab8Test(TestCase):
	def test_lab_8_url_is_exist(self):
		response = Client().get('/story8/')
		self.assertEqual(response.status_code,200)
	def test_lab_8_using_to_do_list_template(self):
		response = Client().get('/story8/')
		self.assertTemplateUsed(response, 'myweb.html')
	def test_lab_8_using_index_func(self):
		found = resolve('/story8/')
		self.assertEqual(found.func, homepage)

"""
class Lab7FuncTest(unittest.TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('--disable-gpu')
		service_log_path = "./chromedriver.log"
		serice_args = ['--verbose']
		self.driver  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(Lab7FuncTest, self).setUp()
	def tearDown(self):
		self.driver.close()
		super(Lab7FuncTest, self).tearDown()

	def test_layout_judul(self):
		browser = self.driver
		browser.get("http://127.0.0.1:8000/")
		judul = browser.find_element_by_class_name("navbar-brand")
		self.assertIn("Story 8",judul.text)


	def test_style_bootstrap(self):
		browser = self.driver
		browser.get("http://127.0.0.1:8000/")
		bs = browser.find_element_by_id("bootstrap")
		self.assertEqual("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css",bs.get_attribute('href'))

	def test_accordion_use_bootstrap(self):
		browser = self.driver
		browser.get("http://127.0.0.1:8000/")
		konten= browser.find_element_by_id("badan-konten")
		self.assertIn("container-fluid",konten.get_attribute('class'))

	def test_style_warna_dan_button(self):
		browser = self.driver
		browser.get("http://127.0.0.1:8000/")
		button = browser.find_element_by_id("button-red")
		button.send_keys(Keys.RETURN)
		body = browser.find_element_by_tag_name("body")
		self.assertEqual("background-color: red;",body.get_attribute('style'))
"""


